package com.phakphoom.awareassginment.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phakphoom on 04/12/17.
 */

public class PhoneNumberCollectionDao implements Parcelable {

    private List<PhoneNumberDao> data;

    public PhoneNumberCollectionDao() {
        data = new ArrayList<>();
    }

    protected PhoneNumberCollectionDao(Parcel in) {
        data = in.createTypedArrayList(PhoneNumberDao.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PhoneNumberCollectionDao> CREATOR = new Creator<PhoneNumberCollectionDao>() {
        @Override
        public PhoneNumberCollectionDao createFromParcel(Parcel in) {
            return new PhoneNumberCollectionDao(in);
        }

        @Override
        public PhoneNumberCollectionDao[] newArray(int size) {
            return new PhoneNumberCollectionDao[size];
        }
    };

    public List<PhoneNumberDao> getData() {
        return data;
    }

    public void setData(List<PhoneNumberDao> data) {
        this.data = data;
    }
}
