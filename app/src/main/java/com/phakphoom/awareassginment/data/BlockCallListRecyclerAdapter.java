package com.phakphoom.awareassginment.data;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.phakphoom.awareassginment.R;
import com.phakphoom.awareassginment.util.Contextor;

/**
 * Created by phakphoom on 04/12/17.
 */

public class BlockCallListRecyclerAdapter extends RecyclerView.Adapter {


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.phone_num_item, parent, false);
        return new PhoneNumberViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final PhoneNumberViewHolder itemView = (PhoneNumberViewHolder) holder;
        itemView.tvPhoneListItem.setText(BlockCallManager.getInstance().getDao().getData().get(position).getPhoneNumber());
        itemView.btnRemovePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setMessage("Confirm to remove the blocked number?")
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        BlockCallManager.getInstance().removePhoneNum(position);
                                        notifyDataSetChanged();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        notifyDataSetChanged();
                                    }
                                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return BlockCallManager.getInstance().getCount();
    }

    public static class PhoneNumberViewHolder extends RecyclerView.ViewHolder {

        public TextView tvPhoneListItem;
        public ImageButton btnRemovePhone;

        public PhoneNumberViewHolder(View itemView) {
            super(itemView);
            this.tvPhoneListItem = (TextView) itemView.findViewById(R.id.tvPhoneListItem);
            this.btnRemovePhone = (ImageButton) itemView.findViewById(R.id.btnRemovePhone);
        }
    }
}
