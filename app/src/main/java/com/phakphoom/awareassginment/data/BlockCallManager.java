package com.phakphoom.awareassginment.data;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.gson.Gson;
import com.phakphoom.awareassginment.util.Contextor;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by phakphoom on 04/12/17.
 */

public class BlockCallManager {

    private Context mContext;
    private static PhoneNumberCollectionDao sCollectionDao;
    private static BlockCallManager instance;


    public static BlockCallManager getInstance() {
        if (instance == null) {
            instance = new BlockCallManager();
        }
        return instance;
    }

    private BlockCallManager() {
        mContext = Contextor.getInstance().getContext();

        loadData();

        if (getDao() == null)
            newDao();
    }

    public PhoneNumberCollectionDao getDao(){
        return sCollectionDao;
    }

    private void newDao(){
        sCollectionDao = new PhoneNumberCollectionDao();
        saveData();
    }

    public int getCount() {
        if (sCollectionDao == null)
            return 0;
        if (sCollectionDao.getData() == null)
            return 0;

        return sCollectionDao.getData().size();
    }

    public void addPhoneNum(PhoneNumberDao newItem){
        Iterator<PhoneNumberDao> iterator = sCollectionDao.getData().iterator();
        boolean checkDup = false;
        while (iterator.hasNext()) {
            if (newItem.getPhoneNumber().contentEquals(iterator.next().getPhoneNumber()))
                checkDup = true;
        }

        if (!checkDup) {
            sCollectionDao.getData().add(newItem);
            saveData();
        } else {
            Toast.makeText(mContext,
                    "Detected duplicate values!!",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void removePhoneNum(final int position) {
        if (sCollectionDao != null) {
            sCollectionDao.getData().remove(position);
            saveData();
        }
    }

    private void saveData(){
        String json = new Gson().toJson(sCollectionDao);
        SharedPreferences prefs = mContext.getSharedPreferences("phonenum",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("json", json);
        editor.apply();
    }

    private void loadData(){
        SharedPreferences prefs = mContext.getSharedPreferences("phonenum",
                Context.MODE_PRIVATE);
        String json = prefs.getString("json", null);
        if (json == null )
            return;
        sCollectionDao = new Gson().fromJson(json, PhoneNumberCollectionDao.class);
    }
}
