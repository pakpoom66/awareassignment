package com.phakphoom.awareassginment.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by phakphoom on 04/12/17.
 */

public class PhoneNumberDao implements Parcelable {

    private String phoneNumber;

    public PhoneNumberDao() {

    }

    protected PhoneNumberDao(Parcel in) {
        phoneNumber = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(phoneNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PhoneNumberDao> CREATOR = new Creator<PhoneNumberDao>() {
        @Override
        public PhoneNumberDao createFromParcel(Parcel in) {
            return new PhoneNumberDao(in);
        }

        @Override
        public PhoneNumberDao[] newArray(int size) {
            return new PhoneNumberDao[size];
        }
    };

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
