package com.phakphoom.awareassginment;

import android.app.Application;

import com.phakphoom.awareassginment.util.Contextor;

/**
 * Created by phakphoom on 04/12/17.
 */

public class BlackCallApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Contextor.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
