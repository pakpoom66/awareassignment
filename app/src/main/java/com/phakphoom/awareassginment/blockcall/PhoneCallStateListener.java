package com.phakphoom.awareassginment.blockcall;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.google.gson.Gson;
import com.phakphoom.awareassginment.data.PhoneNumberCollectionDao;
import com.phakphoom.awareassginment.data.PhoneNumberDao;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * Created by phakphoom on 05/12/17.
 */

public class PhoneCallStateListener extends PhoneStateListener {

    private Context mContext;
    private PhoneNumberCollectionDao sCollectionDao;
    private SharedPreferences prefs;

    public PhoneCallStateListener(Context context){
        this.mContext = context;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {

        prefs = mContext.getSharedPreferences("phonenum",
                Context.MODE_PRIVATE);
        String json = prefs.getString("json", null);
        if (json == null )
            return;
        sCollectionDao = new Gson().fromJson(json, PhoneNumberCollectionDao.class);

        switch (state) {

            case TelephonyManager.CALL_STATE_RINGING:

                AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
                audioManager.setStreamMute(AudioManager.STREAM_RING, true);             //Turn ON the mute
                TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
                Log.d("PhoneIN", "in: "+incomingNumber+" , State: "+ state);
                Iterator<PhoneNumberDao> iterator = sCollectionDao.getData().iterator();
                while (iterator.hasNext()) {
                    String block_number = iterator.next().getPhoneNumber().toString();

                    try {
                        Class aClass = Class.forName(telephonyManager.getClass().getName());
                        Method method = aClass.getDeclaredMethod("getITelephony");
                        method.setAccessible(true);
                        ITelephony telephonyService;

                        //Checking incoming call number
                        if (incomingNumber.equalsIgnoreCase(block_number)) {
                            showToast("Block in : " + block_number);
                            //telephonyService.silenceRinger(); //Security exception problem
                            telephonyService = (ITelephony) method.invoke(telephonyManager);
                            telephonyService.silenceRinger();
                            telephonyService.endCall();
                        }
                    } catch (Exception e) {
                        showToast(e.toString());
                    }
                    //Turn OFF the mute
                    audioManager.setStreamMute(AudioManager.STREAM_RING, false);
                }
                break;
            default:
        }

        super.onCallStateChanged(state, incomingNumber);
    }

    private void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

}
