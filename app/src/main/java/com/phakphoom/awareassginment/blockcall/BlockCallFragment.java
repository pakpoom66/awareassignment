package com.phakphoom.awareassginment.blockcall;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.phakphoom.awareassginment.R;
import com.phakphoom.awareassginment.data.BlockCallListRecyclerAdapter;
import com.phakphoom.awareassginment.data.BlockCallManager;
import com.phakphoom.awareassginment.data.PhoneNumberDao;

public class BlockCallFragment extends Fragment {

    /*************************
     *      Variables
     **************************/

    private EditText editPhoneInput;
    private ImageButton btnAddPhone;

    private RecyclerView blockCallRecyclerView;
    private BlockCallListRecyclerAdapter blockCallListRecyclerAdapter;

    private String phoneNumPattern = "^0[0-9]{9}$";
    private String zeroFirst = "^0[a-zA-Z0-9._-]+$";
    private String tenDigit = "^[0-9]{10}$";


    /***********************************
     *      Methods / Functions
     ************************************/

    public BlockCallFragment() {
        super();
    }

    public static BlockCallFragment newInstance() {
        BlockCallFragment fragment = new BlockCallFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_block_call, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        // Note: State of variable initialized here could not be saved
        //       in onSavedInstanceState

        blockCallRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        blockCallRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        blockCallListRecyclerAdapter = new BlockCallListRecyclerAdapter();
        blockCallRecyclerView.setAdapter(blockCallListRecyclerAdapter);

        editPhoneInput = (EditText) rootView.findViewById(R.id.editPhoneInput);
        btnAddPhone = (ImageButton) rootView.findViewById(R.id.btnAddPhone);
        btnAddPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNum = editPhoneInput.getText().toString();
                if (!phoneNum.matches(zeroFirst)) {
                    editPhoneInput.setError("Must start with '0'");
                } else if (!phoneNum.matches(tenDigit)) {
                    editPhoneInput.setError("Validates the 10 digits");
                }else if (phoneNum.matches(phoneNumPattern)) {
                    PhoneNumberDao dao = new PhoneNumberDao();
                    dao.setPhoneNumber(editPhoneInput.getText().toString());
                    BlockCallManager.getInstance().addPhoneNum(dao);
                    blockCallListRecyclerAdapter.notifyDataSetChanged();
                    editPhoneInput.setText("");
                } else {
                    editPhoneInput.setError("Invalid Phone Number!");
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    /*************************
     *      Listeners
     *************************/



    /*************************
     *      Inner Class
     *************************/



}
